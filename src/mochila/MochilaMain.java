package mochila;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.InputMismatchException;
import java.util.Scanner;


public class MochilaMain {
	public static void main(String args[])
			throws MochilaLlenaExcepcion, DemasiadasPokeballs, DemasiadasPocionesExcepcion {
		Scanner teclado = new Scanner(System.in);
		String especie, alias, nombre, sexo, capturar;
		boolean seguir = true;
		boolean entrar = true;
		int opcion, nivel, especieRandom, dia, mes, anio, pokemoney = 0, decision;

		Objetos pokeballs = new Pokeballs();
		ObjetoPociones pociones = new Pociones();

		/* Variables del case 2 */
		boolean volver = false;
		char opcionListar;
		int pokemonElegido;
		String montar;

		/* Instanciamos los pokemons */
		Caterpie cat;
		Lickitung lick;
		Pidgey pid;
		Pikachu pika;
		Rayquaza ray;
		Squirtle squirt;
		Jugador persona;

		System.out.println("Bienvenido al reino pokemon");
		System.out.print("Dime tu nombre: ");
		nombre = teclado.nextLine();
		System.out.print("Dime tu sexo: ");
		sexo = teclado.nextLine();
		if (sexo.equals("Mujer") || sexo.equals("Hombre")) {
		} else {
			System.out.println("No es un sexo valido.");
			System.out.println("Te jodes, t� sexo es otro.");
			sexo = "Otro";
		}

		System.out.println("Dime cuando naciste(Primero el mes, luego dia y por ultimo a�o): ");

		/* Mes */
		System.out.print("Dime el mes: ");
		mes = teclado.nextInt();
		if (mes <= 1 || mes >= 13) {
			System.out.println("Ese mes no es valido");
			System.out.println("Vuelve a introducirlo (Solo tienes 1 intento)");
			mes = teclado.nextInt();
			if (mes <= 1 || mes >= 13) {
				System.out.println("Ya que no sabes poner fechas te la pongo yo");
				mes = (int) (Math.random() * 12 + 1);
			}
		}

		/* Dia */
		System.out.print("Dime el dia: ");
		dia = teclado.nextInt();
		if (mes == 2) {
			if (dia <= 1 || dia >= 28) {
				System.out.println("No es un dia valido");
				System.out.println("Vuelve a introducirlo (Solo tienes 1 intento)");
				dia = teclado.nextInt();
				if (dia <= 1 || dia >= 28) {
					System.out.println("Ya que no sabes poner fechas te la pongo yo");
					dia = (int) (Math.random() * 29 + 1);
				}
			}
		} else {
			if (dia <= 1 || dia >= 31) {
				System.out.println("No es un d�a valido");
				System.out.println("Vuelve a introducirlo (Solo tienes 1 intento)");
				dia = teclado.nextInt();
				if (dia <= 1 || dia >= 31) {
					System.out.println("Ya que no sabes poner fechas te la pongo yo");
					dia = (int) (Math.random() * 31 + 1);
				}
			}
		}

		/* Anio */
		System.out.print("Dime el anio: ");
		anio = teclado.nextInt();
		if (anio <= 1000 || anio >= 2019) {
			System.out.println("No es un a�o valido");
			System.out.println("Vuelve a introducirlo (Solo tienes 1 intento)");
			anio = teclado.nextInt();
			if (anio <= 1000 || anio >= 2019) {
				System.out.println("Ya que no sabes poner fechas te la pongo yo");
				anio = 2019;
			}
		}

		persona = new Jugador(nombre, sexo, dia, mes, anio, pokemoney);
		persona.aniadirPokeballs(pokeballs, 10);

		while (seguir) {
			System.out.println("========");
			System.out.println("| MENU |");
			System.out.println("========");
			System.out.println("1) Capturar Pokemon");
			System.out.println("2) Listar Pokemon");
			System.out.println("3) Mostrar Informaci�n");
			System.out.println("4) Tienda");
			System.out.println("0) Salir");
			System.out.println("");

			try {
				System.out.print("Opci�n: ");
				opcion = teclado.nextInt();
				teclado.nextLine(); /* Eliminamos el retorno de carro para no tener problemas a posteriori */

				switch (opcion) {
				case 1:
					/* Sacamos aleatoriamente el nivel del pokemon y su especie */
					System.out.println(persona.getNombre() + " lanza una pokeball");

					while (entrar) {
						float numerocapturaaleatorio = (float) (Math.random() * 1); /* Porcentaje captura aleatorio */
						if (numerocapturaaleatorio < 0.33) {
							persona.quitarPokeballs("Pokeball");
							nivel = (int) (Math.random() * 101);
							especieRandom = (int) (Math.random() * 6);

							if (especieRandom == 1) {
								especie = "Pikachu";
							} else if (especieRandom == 2) {
								especie = "Squirtle";
							} else if (especieRandom == 3) {
								especie = "Pidgey";
							} else if (especieRandom == 4) {
								especie = "Caterpie";
							} else if (especieRandom == 5) {
								especie = "Rayquaza";
							} else {
								especie = "Lickitung";
							}
							System.out.println("=====================");
							System.out.println("| Pok�mon capturado |");
							System.out.println("=====================");
							System.out.println("Especie: " + especie);
							System.out.println("Nivel: " + nivel);
							System.out.print("Mote: ");
							alias = teclado.nextLine();
							System.out.println("");
							try {
								if (especieRandom == 1) {
									if (alias.equals("")) {
										alias = "Pikachu";
									}
									pika = new Pikachu(alias, nivel);
									persona.aniadirPokemon(pika);
								} else if (especieRandom == 2) {
									if (alias.equals("")) {
										alias = "Squirtle";
									}
									squirt = new Squirtle(alias, nivel);
									persona.aniadirPokemon(squirt);
								} else if (especieRandom == 3) {
									if (alias.equals("")) {
										alias = "Pidgey";
									}
									pid = new Pidgey(alias, nivel);
									persona.aniadirPokemon(pid);
								} else if (especieRandom == 4) {
									if (alias.equals("")) {
										alias = "Caterpie";
									}
									cat = new Caterpie(alias, nivel);
									persona.aniadirPokemon(cat);
								} else if (especieRandom == 5) {
									if (alias.equals("")) {
										alias = "Rayquaza";
									}
									ray = new Rayquaza(alias, nivel);
									persona.aniadirPokemon(ray);
								} else {
									if (alias.equals("")) {
										alias = "Lickitung";
									}
									lick = new Lickitung(alias, nivel);
									persona.aniadirPokemon(lick);
								}
							} catch (MochilaLlenaExcepcion e) {
								System.err.println(e.toString());
							}

							persona.setPokemoney(persona.getPokemoney() + 300);/* A�adimos 300 pokemonedas */
							entrar = false;
						} else {
							/* No capturamos al pokemon y pregunta si queremos volver a capturarlo */
							System.out.println("Hay que calvario, no lo has capturado");
							System.out.println("Tienes: " + persona.getCantidadPokeballs() + " pokeballs");
							System.out.println("�Quieres intentarlo? S/N");
							capturar = teclado.nextLine();
							if (capturar.equals("S") || capturar.equals("s")) {
								if (persona.getCantidadPokeballs() > 0) {
									System.out.println("Vale volvamos a intentarlo");
									persona.quitarPokeballs("Pokeball");
									entrar = true;
								} else {
									System.out.println("No te quedan pokeballs");
									entrar = false;
								}
							} else {
								System.out.println("Adios");
								entrar = false;
							}
						}
					}
					entrar = true;
					break;

				case 2:
					volver = false;
					/* Llamamos a la funci�n para listar los Pok�mon */
					if (persona.getCantidad() == 0) {
						System.err.println("�No tienes ning�n Pok�mon!");
					} else {
						while (!volver) {
							System.out.println("============");
							System.out.println("| Pok�mons |");
							System.out.println("============");
							for (int i = 0; i < persona.getCantidad(); i++) {
								System.out.println((i + 1) + " " + persona.getPokemon(i).getNombre() + " ("
										+ persona.getPokemon(i).getClass().getSimpleName() + ") - lvl "
										+ persona.getPokemon(i).getNivel());
							}
							System.out.print("Opciones (M - Mostrar; L - Liberar; V - Volver): ");
							opcionListar = teclado.next().charAt(0);
							switch (opcionListar) {
							case 'M':
								try {
									System.out.print("�Qu� Pok�mon quieres consultar? ");
									pokemonElegido = teclado.nextInt() - 1;
									System.out.println("");
									System.out.println(persona.getPokemon(pokemonElegido).getNombre());
									System.out.println("==============");
									System.out.println("�Especie: "
											+ persona.getPokemon(pokemonElegido).getClass().getSimpleName());
									System.out.println("�Nivel: " + persona.getPokemon(pokemonElegido).getNivel());
									System.out.print("Tipo: ");
									for (int i = 0; i < persona.getPokemon(pokemonElegido).getTipo().length; i++) {
										System.out.print(persona.getPokemon(pokemonElegido).getTipo()[i] + " ");
									}
									System.out.println("");
									System.out.println("�PS: " + persona.getPokemon(pokemonElegido).getPs());
									System.out.println("�Ataque: " + persona.getPokemon(pokemonElegido).getAtaque());
									System.out.println("�Defensa: " + persona.getPokemon(pokemonElegido).getDefensa());
									System.out.println(
											"Hora captura: " + persona.getPokemon(pokemonElegido).getMomentoActual());
									System.out.println("");

									// Guardamos en un array las posibles interfaces del pokemon elegido
									Class[] i = persona.getPokemon(pokemonElegido).getClass().getInterfaces();
									try {
										if (i[0] != null) {
											System.out.println("�Quieres montar a "
													+ persona.getPokemon(pokemonElegido).getNombre() + "? Y/N");
											montar = teclado.next();
											if (montar.equals("Y") || montar.equals("y")) {
												System.out.println(
														((Pokemontura) (persona.getPokemon(pokemonElegido))).montar());
											}
										}
									} catch (ArrayIndexOutOfBoundsException e) {
										// No ponemos nada ya que esta excepci�n saltar� cuando mostremos un Pokemon que
										// no tiene la habilidad de montar
									}

								} catch (NullPointerException e) {
									System.err.println("�Pok�mon no encontrado!");
								}
								break;

							case 'L':
								try {
									System.out.print("�Qu� Pok�mon quieres liberar? ");
									pokemonElegido = teclado.nextInt();
									if (persona.getCantidad() < pokemonElegido) {
										System.err.println("�Pok�mon no encontrado!");
									} else {
										System.out.println("");
										persona.liberarPokemon(pokemonElegido - 1);
										System.out.println("�Has liberado al Pok�mon!");
									}
								} catch (NullPointerException e) {
									System.err.println("�Pok�mon no encontrado!");
								}
								break;

							case 'V':
								volver = true;
								break;
							}
						}
					}
					break;

				case 3:
					/* Mostrar informaci�n persona */
					System.out.println("Estos son tus datos");
					System.out.println("Nombre: " + persona.getNombre());
					System.out.println("Sexo: " + persona.getSexo());
					System.out.println(
							"Nacimiento: " + persona.getDia() + "/" + persona.getMes() + "/" + persona.getAnio());
					System.out.println("Pokemoney: " + persona.getPokemoney());
					System.out.println("Pokeballs: " + persona.getCantidadPokeballs());
					System.out.println("Pociones: " + persona.getCantidadPociones());

					break;

				case 4:
					/* Comprar pokeballs */
					System.out.println("Bienvenido a la tienda");
					System.out.println("�Quieres comprar pokeballs?");
					System.out.println("1)Comprar pokeballs 2)Comprar Pociones 3)Salir");
					System.out.println("Recuerda que cada pokeball cuesta 100 pokemoney");
					decision = teclado.nextInt();
					switch (decision) {
					case 1:
						try {
							/* Decision del usuario y mira si tienes dinero y espacio en la mochila */
							System.out.print("Cuantas pokeballs deseas comprar: ");
							int cantidad = teclado.nextInt();
							if (cantidad > persona.getPokemoney() / 100) {
								System.out.println("No tienes suficiente dinero");
							} else {
								if (persona.getCantidadPokeballs() > 50) {
									System.out.println("Demasiadas pokeballs");
								} else {
									int dineroquitar = cantidad * 100;
									persona.aniadirPokeballs(pokeballs, cantidad);
									persona.setPokemoney(persona.getPokemoney() - dineroquitar);
									System.out.println("Tienes este dinero: " + persona.getPokemoney());
								}
							}
						} catch (DemasiadasPokeballs ex) {
							System.err.println(ex.toString());
						}
						break;
					case 2:
						try {
							/* Decision del usuario y mira si tienes dinero y espacio en la mochila */
							System.out.print("Cuantas pociones deseas comprar: ");
							int cantidad = teclado.nextInt();
							if (cantidad > persona.getPokemoney() / 30) {
								System.out.println("No tienes suficiente dinero");
							} else {
								if (persona.getCantidadPociones() > 50) {
									System.out.println("Demasiadas pociones");
								} else {
									int dineroquitar = cantidad * 30;
									persona.aniadirPociones(pociones, cantidad);
									persona.setPokemoney(persona.getPokemoney() - dineroquitar);
									System.out.println("Tienes este dinero: " + persona.getPokemoney());
								}
							}
						} catch (DemasiadasPocionesExcepcion po) {
							System.err.println(po.toString());
						}
						break;
					}

					break;
				case 0:
					seguir = false;
					System.out.println("�Hasta la pr�xima, entrenador!");
					break;
				}
			} catch (InputMismatchException e) {
				System.err.println("�Debes introducir un n�mero!");
				teclado.next();
			}
		}
	}
}
