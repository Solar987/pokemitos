package mochila;

public class Squirtle extends Pokemon {
	
	/**
	 * Valores del pokemon
	 * @param nombre
	 * @param nivel
	 */
	
	public Squirtle(String nombre, int nivel) {
		this.tipo = new String[1];
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo[0] = "Agua";
		this.ps = nivel * 13;
		this.ataque = nivel * 25;
		this.defensa = nivel * 2;
		
	}
}
