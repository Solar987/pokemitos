package mochila;

public class Caterpie extends Pokemon implements Pokemontura{
	
	/**
	 * Valores del pokemon
	 * @param nombre
	 * @param nivel
	 */
	
	public Caterpie(String nombre, int nivel) {
		this.tipo = new String[1];
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo[0] = "Bicho";
		this.ps = nivel * 1;
		this.ataque = nivel * 1;
		this.defensa = nivel * 1;
	}

	@Override
	public String montar() {
		String salida = "�Te has montado en " + nombre + " correctamente ;) !";
		return salida;
	}
}
