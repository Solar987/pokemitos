package mochila;

/**
 * 
 * @author alvaro
 * Clase mochila
 */

public class Mochila {

	private Pokemon[] mochila;
	private Objetos[] objeto;
	private ObjetoPociones[] objetopociones;

	private int cantidad;
	private int cantidadobjetos;
	private int cantidadpociones;
	

	public Mochila() {
		mochila = new Pokemon[51];
		objeto = new Objetos[51];
		objetopociones = new ObjetoPociones[51];
		cantidad = 0;
		cantidadobjetos = 0;
		cantidadpociones = 0;
	}
	
	/**
	 * 
	 * @param posicion
	 * @return
	 * Devuelve la posicion del pokemon en la mochila 
	 */
	
	public Pokemon getPokemon(int posicion) {
		return mochila[posicion];
	}
	
	/**
	 * Metodo para liberar al los pokemons
	 * @param posicion
	 */
	
	public void liberarPokemon(int posicion) {
		for (int i = posicion; i <= cantidad; i++) {
			mochila[posicion] = mochila[posicion + 1];
		}
		cantidad--;
	}
	
	/**
	 * Metodo para aniadir a los pokemons
	 * @param nuevo
	 * @throws MochilaLlenaExcepcion
	 */
	
	public void aniadirPokemon(Pokemon nuevo) throws MochilaLlenaExcepcion {
		try {
			mochila[cantidad] = nuevo;
			cantidad++;
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new MochilaLlenaExcepcion(cantidad);
		}
	}
	
	/**
	 * Metodo para devolver la cantidad
	 * @return
	 */
	
	public int getCantidad() {
		return cantidad;
	}

	/* Las pokeballs */
	
	/**
	 * Metodo para aniadir las pokeballs
	 * @param objetoAniadir
	 * @param cantidadAniadir
	 * @throws DemasiadasPokeballs
	 */
	
	public void aniadirPokeballs(Objetos objetoAniadir, int cantidadAniadir) throws DemasiadasPokeballs {
		try {
			for (int i = 0; i < cantidadAniadir; i++) {
				objeto[cantidadobjetos] = objetoAniadir;
				cantidadobjetos++;
			}
		} catch (ArrayIndexOutOfBoundsException p) {
			throw new DemasiadasPokeballs(cantidadobjetos);
		}
	}

	/**
	 * Metodo para quitar pokeballs
	 * @param objetos
	 */
	
	public void quitarPokeballs(String objetos) {
		int borramos = 0;
		for (int i = 0; i < cantidadobjetos; i++) {
			if (objeto[i].getNombre().equals(objetos)) {
				borramos = i;
			}
		}
		for (int i = borramos; i <= cantidadobjetos; i++) {
			objeto[borramos] = objeto[borramos + 1];
		}
		cantidadobjetos--;
	}
	
	/**
	 * Metodo para ver la cantidad de pokeballs que tenemos
	 * @return
	 */
	
	public int getCantidadPokeballs() {
		int cantidadpokeball = 0;
		for (int i = 0; i < cantidadobjetos; i++) {
			if (objeto[i].getNombre().equals("Pokeball")) {
				cantidadpokeball++;
			}
		}
		return cantidadpokeball;
	}

	public Objetos getObjetos(int i) {
		return objeto[i];
	}

	/* Las pociones */
	
	/**
	 * Metodo para aniadir pociones
	 * @param objetoAniadirPocion
	 * @param cantidadAniadirPocion
	 * @throws DemasiadasPocionesExcepcion
	 */
	
	public void aniadirPociones(ObjetoPociones objetoAniadirPocion, int cantidadAniadirPocion)
			throws DemasiadasPocionesExcepcion {
		try {
			for (int i = 0; i < cantidadAniadirPocion; i++) {
				objetopociones[cantidadpociones] = objetoAniadirPocion;
				cantidadpociones++;
			}
		} catch (ArrayIndexOutOfBoundsException p) {
			throw new DemasiadasPocionesExcepcion(cantidadpociones);
		}
	}
	
	/**
	 * Metodo para quitar pociones
	 * @param objetospociones
	 */
	
	public void quitarPociones(String objetospociones) {
		int borramos = 0;
		for (int i = 0; i < cantidadpociones; i++) {
			if (objetopociones[i].getNombrePociones().equals(objetospociones)) {
				borramos = i;
			}
		}
		for (int i = borramos; i <= cantidadpociones; i++) {
			objetopociones[borramos] = objetopociones[borramos + 1];
		}
		cantidadpociones--;
	}

	/**
	 * Metodo para ver la cantidad de pociones que tenemos
	 * @return
	 */
	
	public int getCantidadPociones() {
		int cantidadpocione = 0;
		for (int i = 0; i < cantidadpociones; i++) {
			if (objetopociones[i].getNombrePociones().equals("Pociones")) {
				cantidadpocione++;
			}
		}
		return cantidadpocione;
	}

	public ObjetoPociones getObjetoPociones(int e) {
		return objetopociones[e];
	}

}
