package mochila;

public class Pikachu extends Pokemon {
	
	/**
	 * Valores del pokemon
	 * @param nombre
	 * @param nivel
	 */
	
	public Pikachu(String nombre, int nivel) {
		this.tipo = new String[1];
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo[0] = "Electrico";
		this.ps = nivel * 20;
		this.ataque = nivel * 5;
		this.defensa = nivel * 7;

	}
}