package mochila;

public abstract class ObjetoPociones {
	protected String nombrepociones;
	
	/**
	 * Recoje el nombre del objeto pocion
	 * @return
	 */
	
	public String getNombrePociones() {
		return nombrepociones;
	}
	
	/**
	 * Estableces el nombre que le pasa el usuario
	 * @param nombrepociones
	 */

	public void setNombrePociones(String nombrepociones) {
		this.nombrepociones = nombrepociones;
	}
}
