package mochila;

public abstract class Objetos {
	protected String nombre;
	
	/**
	 * Recoje el nombre del objeto pokeball
	 * @return
	 */
	
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * Estableces el nombre que le pasa el usuario
	 * @param nombre
	 */
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
