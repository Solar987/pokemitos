package mochila;

public class Rayquaza extends Pokemon {
	
	/**
	 * Valores del pokemon
	 * @param nombre
	 * @param nivel
	 */
	
	public Rayquaza(String nombre, int nivel) {
		this.tipo = new String[2];
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo[0] = "Volador";
		this.tipo[1] = "Dragon";
		this.ps = nivel * 99;
		this.ataque = nivel * 99;
		this.defensa = nivel * 99;
	}
}
