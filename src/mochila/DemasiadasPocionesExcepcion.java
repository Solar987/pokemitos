package mochila;

public class DemasiadasPocionesExcepcion extends Exception{
	private int cantidadpociones;
	
	/**
	 * 	Excepcion para las pociones
	 * @param cantidadpociones
	 */
	
	public DemasiadasPocionesExcepcion (int cantidadpociones) {
		this.cantidadpociones = cantidadpociones;
	}
	
	/**
	 * Mensaje de la excepcion
	 */
	
	@Override
    public String toString () {
        return "�Mochila llena! No puedes guardar m�s Pociones." 
        		+ "\n" + "El tama�o m�ximo de la mochila es "
        		+ cantidadpociones + ". \n";
    }
}
