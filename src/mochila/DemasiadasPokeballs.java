package mochila;

public class DemasiadasPokeballs extends Exception{
	private int cantidadobjetos;
	
	/**
	 * Excepcion para las pokeballs
	 * @param cantidadobjetos
	 */
	
	public DemasiadasPokeballs (int cantidadobjetos) {
		this.cantidadobjetos = cantidadobjetos;
	}
	
	/**
	 * Mensaje de la excepcion
	 */
	
	@Override
    public String toString () {
        return "�Mochila llena! No puedes guardar mas Pokeballs." 
        		+ "\n" + "El tama�o m�ximo de la mochila es "
        		+ cantidadobjetos + ". \n";
    }
}
