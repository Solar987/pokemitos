package mochila;

public class MochilaLlenaExcepcion extends Exception{
	private int cantidad;
	
	/**
	 * Excepcion para la mochila
	 * @param cantidad
	 */
	
	public MochilaLlenaExcepcion (int cantidad) {
		this.cantidad = cantidad;
	}
	
	/**
	 * Mensaje de la excepcion
	 */
	
	@Override
    public String toString () {
        return "Mochila llena No puedes guardar m�s Pok�mons." 
        		+ "\n" + "El tamanio maximo de la mochila es "
        		+ cantidad + ". \n";
    }
}
