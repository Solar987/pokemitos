package mochila;

import java.sql.Timestamp;
import java.util.Date;

public abstract class Pokemon {
	protected String nombre;
	protected int nivel;
	protected String[] tipo;
	protected int ps;
	protected int ataque;
	protected int defensa;
	protected Timestamp tiempoAhora = new Timestamp(new Date().getTime());
	
	/**
	 * Devuelve el nombre del pokemon
	 * @return
	 */
	
	public String getNombre() {
		return nombre;
	}

	/**
	 * Devuelve el nivel del pokemon
	 * @return
	 */
	
	public int getNivel() {
		return nivel;
	}

	/**
	 * Devuelve el tipo del pokemon
	 * @return
	 */
	
	public String[] getTipo() {
		return tipo;
	}

	/**
	 * Devuelve la vida del pokemon
	 * @return
	 */
	
	public int getPs() {
		return ps;
	}

	/**
	 * Devuelve el ataque del pokemon
	 * @return
	 */
	
	public int getAtaque() {
		return ataque;
	}

	/**
	 * Devuelve la defensa del pokemon
	 * @return
	 */
	
	public int getDefensa() {
		return defensa;
	}

	/**
	 * Devuelve el momento en el que has capturado al pokemon
	 * @return
	 */
	
	public Timestamp getMomentoActual() {
		return tiempoAhora;
	}

}
