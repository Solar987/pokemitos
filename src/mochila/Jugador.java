package mochila;

public class Jugador {
	String nombre;
	String sexo;
	int dia;
	int mes;
	int anio;
	int pokemoney;
	int cantidadobjetos;
	int cantidadpociones;
	Mochila mochila = new Mochila();

	/**
	 * 
	 * @param nombre
	 * @param sexo
	 * @param dia
	 * @param mes
	 * @param anio
	 * @param pokemoney
	 * Recoje los datos del jugador
	 */
	public Jugador(String nombre, String sexo, int dia, int mes, int anio, int pokemoney) {
		this.nombre = nombre;
		this.sexo = sexo;
		this.dia = dia;
		this.mes = mes;
		this.anio = anio;
		this.pokemoney = 0;
	}

	/**
	 * 
	 * @return nombre
	 */

	public String getNombre() {
		return nombre;
	}

	/**
	 * 
	 * @return sexo
	 */

	public String getSexo() {
		return sexo;
	}

	/**
	 * 
	 * @return dia
	 */

	public int getDia() {
		return dia;
	}

	/**
	 * 
	 * @return mes
	 */

	public int getMes() {
		return mes;
	}

	/**
	 * 
	 * @return anio
	 */

	public int getAnio() {
		return anio;
	}

	/**
	 * 
	 * @return pokemoney
	 */

	public int getPokemoney() {
		return pokemoney;
	}

	/**
	 * 
	 * @param pokemoney
	 */

	public void setPokemoney(int pokemoney) {
		this.pokemoney = pokemoney;
	}

	/* Los pokemons */

	/**
	 * 
	 * @param posicion
	 * Llama a mochila para liberar al pokemon
	 */

	public void liberarPokemon(int posicion) {
		this.mochila.liberarPokemon(posicion);
	}

	/**
	 * 
	 * @param nuevo
	 * @throws MochilaLlenaExcepcion
	 * Llama a mochila para aniadir al pokemon
	 */

	public void aniadirPokemon(Pokemon nuevo) throws MochilaLlenaExcepcion {
		this.mochila.aniadirPokemon(nuevo);
		;
	}

	/**
	 * 
	 * @param posicion
	 * @return
	 */
	
	public Pokemon getPokemon(int posicion) {
		return this.mochila.getPokemon(posicion);
	}

	/**
	 * 
	 * @return
	 * Llama a mochila para ver la cantiadad de pokemons
	 */
	
	public int getCantidad() {
		return this.mochila.getCantidad();
	}

	/* Las pokeballs */
	
	/**
	 * Aniade pokeballs
	 * @param objeto
	 * @param cantidadAniadir
	 * @throws DemasiadasPokeballs
	 */
	
	public void aniadirPokeballs(Objetos objeto, int cantidadAniadir) throws DemasiadasPokeballs {
		this.mochila.aniadirPokeballs(objeto, cantidadAniadir);
	}
	
	/**
	 * 
	 * @param objetos
	 * Llama a mochila para quitar pokeballs
	 */
	
	public void quitarPokeballs(String objetos) {
		this.mochila.quitarPokeballs(objetos);
	}

	/**
	 * 
	 * @return devuelve la cantidad de pokeballs desde mochila
	 */
	
	public int getCantidadPokeballs() {
		return this.mochila.getCantidadPokeballs();
	}

	/**
	 * 
	 * @param i
	 * @return
	 */
	
	public Objetos getObjeto(int i) {
		return this.mochila.getObjetos(i);
	}
	
	/* Las pociones */

	/**
	 * Aniade pociones
	 * @param objetopociones
	 * @param cantidadAniadirPocion
	 * @throws DemasiadasPocionesExcepcion
	 */
	
	public void aniadirPociones(ObjetoPociones objetopociones, int cantidadAniadirPocion)
			throws DemasiadasPocionesExcepcion {
		this.mochila.aniadirPociones(objetopociones, cantidadAniadirPocion);
	}

	/**
	 * 
	 * @param objetopociones
	 */
	
	public void quitarPociones(String objetopociones) {
		this.mochila.quitarPociones(objetopociones);
	}

	/**
	 * 
	 * @return devuelve la cantidad de pociones desde mochila
	 */
	
	public int getCantidadPociones() {
		return this.mochila.getCantidadPociones();
	}

	/**
	 * 
	 * @param e
	 * @return
	 */
	
	public ObjetoPociones getObjetoPociones(int e) {
		return this.mochila.getObjetoPociones(e);
	}

}
