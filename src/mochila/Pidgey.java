package mochila;

public class Pidgey extends Pokemon implements Pokemontura{
	
	/**
	 * Valores del pokemon
	 * @param nombre
	 * @param nivel
	 */
	
	public Pidgey (String nombre, int nivel) {
		this.tipo = new String[2];
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo[0] = "Volador";
		this.tipo[1] = "Normal";
		this.ps = nivel * 15;
		this.ataque = nivel * 2;
		this.defensa = nivel * 32;
	}
	
	@Override
	public String montar() {
		String salida = "�Te has montado en " + nombre + " correctamente ;) !";
		return salida;
	}
}
