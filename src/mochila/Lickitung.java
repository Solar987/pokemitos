package mochila;

public class Lickitung extends Pokemon{
	
	/**
	 * Valores del pokemon
	 * @param nombre
	 * @param nivel
	 */
	
	public Lickitung(String nombre, int nivel) {
		this.tipo = new String[1];
		this.nombre = nombre;
		this.nivel = nivel;
		this.tipo[0] = "Normal";
		this.ps = nivel * 12;
		this.ataque = nivel * 2;
		this.defensa = nivel * 5;
	}
}
